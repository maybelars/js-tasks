let btn = document.getElementById("btn");
let elems = document.getElementsByTagName('input');

btn.addEventListener('click', func);

function func(){
    let sum = 0;

    for(let i = 0; i < elems.length; i++){
        sum += +elems[i].value;
    }


    let newInput = document.createElement('input');
    newInput.value = sum;
    let parent = btn.parentElement;
    parent.appendChild(newInput);
}