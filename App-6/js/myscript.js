let elem = document.getElementById('input');
elem.addEventListener('blur', func);

function upto(str){
    let arr = str.split(' ');
    for(i=0;i<arr.length; i++){
        arr[i] = arr[i].split('');
        //console.log(arr[i]);
        //console.log(arr);
        arr[i][0] = arr[i][0].toUpperCase();
        arr[i] = arr[i].join('');
    }
    str = arr.join(' ');
    return str
}

function func(){
    elem.value = upto(elem.value);
}