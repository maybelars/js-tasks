let input = document.querySelector('.num');
let pr = document.querySelector('.pr');

input.addEventListener('blur', func);

function func(){
    let sum = 0;
    let str = input.value;
    let arr = str.split('');

    for(let i = 0; i < arr.length; i++){
        sum += +arr[i];
    }

    pr.innerHTML = sum;
}