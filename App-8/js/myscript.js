let input = document.getElementById('words')
let pr = document.getElementById('result');

input.addEventListener('blur', func);

function func(){
    let max = 0
    let str = input.value;
    let arr = str.split(' ');
    
    for(let i = 0; i < arr.length; i++){
        
        let num = 0;
        arr[i] = arr[i].split('');
        num = arr[i].length;
        if (num > max){
            max = num;
        }
        
    }
    console.log(arr);
    pr.innerHTML = max;
}