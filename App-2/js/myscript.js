let btn = document.getElementById("btn");
let inputs = document.getElementsByClassName('num');
let pr = document.getElementById('result');

btn.addEventListener('click', func);

function func(){
    let sum = 0;

    for(let i = 0; i < inputs.length; i++){
        sum += +inputs[i].value;
    }

    pr.innerHTML = sum;
}